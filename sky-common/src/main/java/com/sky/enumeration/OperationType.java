package com.sky.enumeration;


//枚举
/**
 * 数据库操作类型
 */
public enum OperationType {

    //指定数据库操作类型

    /**
     * 更新操作
     */
    UPDATE,

    /**
     * 插入操作
     */
    INSERT

}
