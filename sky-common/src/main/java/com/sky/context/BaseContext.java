package com.sky.context;

//线程存储空间


public class BaseContext {

    public static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id) {
        threadLocal.set(id);
        //存值
    }

    public static Long getCurrentId() {
        return threadLocal.get();
        //调用get方法
    }

    public static void removeCurrentId() {
        threadLocal.remove();
        //移除
    }

}
