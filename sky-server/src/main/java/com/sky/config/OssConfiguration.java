package com.sky.config;

import com.sky.properties.AliOssProperties;
import com.sky.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * 配置类，用于创建AliOssUtil对象
 * */
@Configuration
//配置类
@Slf4j

public class OssConfiguration {

    @Bean
    //运行项目的时候会创建AliOssUtil对象，并注入到IOC容器之中
    @ConditionalOnMissingBean
    //只有在IOC容器中没有这个对象时，才会创建这个对象
    public AliOssUtil aliOssUtil(AliOssProperties aliOssProperties) {

        log.info("开始创建阿里云文件上传工具类对象{}", aliOssProperties);
        //采用参数注入的方式把参数注入
        return new AliOssUtil(aliOssProperties.getEndpoint(), aliOssProperties.getAccessKeyId(),
                aliOssProperties.getAccessKeySecret(), aliOssProperties.getBucketName());

    }
}

