package com.sky.aspect;


import com.sky.annotation.AutoFill;

import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

import static com.sky.constant.AutoFillConstant.*;


//自定义切面类，实现公共字段的自动填充处理逻辑
@Aspect
//切面类
@Component
//注入IOC容器
@Slf4j
public class AutoFillAspect {
    //切入点--指定作用位置
    @Pointcut("execution(* com.sky.mapper.*.*(..)) && @annotation(com.sky.annotation.AutoFill)")
    //                                所有的类，所有的方法，所有的参数类型
    //切入点要同时满足在mapper包的下面，还要满足里面的字段加入了@AutoFill注解才行
    public void autoFillPointCut() {
    }


    //通知--就是aop输出的内容
    @Before("autoFillPointCut()")
    public void autoFill(JoinPoint joinPoint) {
        //                连接点
        log.info("开始进行公共字段自动填充。。。");


        //1，获取当前被拦截的方法的数据库类型
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //singnture是获取对象的详细信息,例如方法名，方法参数，方法返回值等
        //但是不能返回方法的参数类型，返回类型等详细的信息
        // MethodSignature是Signature的子类，完善了相应的信息
        AutoFill autoFill = signature.getMethod().getAnnotation(AutoFill.class);
        //获得方法的注解对象
        //首先是调用getMethod()来获取对象，后面的getAnnotation又注明了要操作的是注解对应的对象
        OperationType operationType = autoFill.value();
        //获取数据库操作的数据类型


        //获取方法的参数--实体对象
        Object[] args = joinPoint.getArgs();
        //获取所有方法参数
        if (args == null || args.length == 0) {
            return;
            //判断参数是否为空
        }


        Object entity = args[0];
        //获取实体对象，Object是所有类的父类


        //准备赋值的数据
        LocalDateTime now = LocalDateTime.now();
        Long currentId = BaseContext.getCurrentId();



        //根据不同操作类型进行赋值
        if (operationType == OperationType.INSERT) {
            //获取四个字段并填充
            try {
                //反射三要素，调用什么对象，形参是什么，返回是什么
                //        返回值          获取文件对象的方法       回去方法名               获取形参
                Method setCreateTime = entity.getClass().getDeclaredMethod(SET_CREATE_TIME, LocalDateTime.class);
                Method setCreateUser = entity.getClass().getDeclaredMethod(SET_CREATE_USER, Long.class);
                Method setUpdateTime = entity.getClass().getDeclaredMethod(SET_UPDATE_TIME, LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod(SET_UPDATE_USER, Long.class);

                //通过反射给对象赋值
                setCreateTime.invoke(entity, now);
                setCreateUser.invoke(entity, currentId);
                setUpdateTime.invoke(entity, now);
                setUpdateUser.invoke(entity, currentId);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        } else if (operationType == OperationType.UPDATE) {
            //获取两个字段并填充
            try {
                //反射三要素，调用什么对象，形参是什么，返回是什么
                //        返回值          获取文件对象的方法       回去方法名               获取形参

                Method setUpdateTime = entity.getClass().getDeclaredMethod(SET_UPDATE_TIME, LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod(SET_UPDATE_USER, Long.class);

                //通过反射给对象赋值

                setUpdateTime.invoke(entity, now);
                setUpdateUser.invoke(entity, currentId);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    //复用的字段需要利用aop和反射填充进去
//    //    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime createTime;
//
//    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime updateTime;
//
//    private Long createUser;
//
//    private Long updateUser;


    //        //1，获取当前被拦截的方法的数据库类型
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();//获取方法签名对象
//        AutoFill autoFill = signature.getMethod().getAnnotation(AutoFill.class);//获得方法上的注解对象
//        OperationType operationType = autoFill.value();//获得数据操作类型
//        //2，获取被拦截的参数--实体对象
//        Object[] args = joinPoint.getArgs();
//        if(args == null || args.length == 0){
//            return;
//        }
//        Object object =  args[0];
//
//        //3，准备赋值的数据，
//        LocalDateTime now = LocalDateTime.now();
//        Long currentId = BaseContext.getCurrentId();
//
//        //4，根据不同操作类型，为对应的属性进行赋值
//        if(operationType == OperationType.INSERT){
//            Entity.getClass().getDeckaredMethod("setCreaTime",LocalDateTime,class);
//
//            //为四个公共字段赋值，多了创建时间，和创建人的id
//
//
//        }else if(operationType == OperationType.UPDATE){
//            //为两个公共字段填充
//


//        }


}



